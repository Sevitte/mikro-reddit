const LocalStrategy = require('passport-local').Strategy;
const {
    pool
} = require("./dbConfig");
const bcrypt = require('bcryptjs');

function initialize(passport) {
    const authenticateUser = (username, password, done, next) => {
        pool.query(
            `SELECT * from users WHERE username = $1`, [username], async(err,
                result) => {
                if (err) throw err;
                if (result.rows.length > 0) {
                    const user = result.rows[0];
                    try {
                        if (await bcrypt.compare(password, user.password)) {
                            return done(null, user);
                        } else {
                            return done(null, false, {
                                message: "Password incorrect"
                            });
                        }
                    } catch (e) {
                        return done(e);
                    }
                } else {
                    return done(null, false, {
                        message: "No user found"
                    });
                }
            }
        );
    };
    passport.use(new LocalStrategy({
        usernameField: 'username'
    }, authenticateUser));
    passport.serializeUser((user, done) => {
        done(null, user.id);
    });
    passport.deserializeUser((id, done) => {
        pool.query(
            `SELECT * from users WHERE id = $1`, [id], (err, result) => {
                if (err) throw err;
                return done(null, result.rows[0]);
            }
        );
    });
}
module.exports = initialize;