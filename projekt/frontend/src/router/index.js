import { createRouter, createWebHistory } from 'vue-router'
import Home from '../components/Home.vue' //to sobie za chwile stworzymy
import List from '../components/List.vue' //to sobie za chwile stworzymy
const router = createRouter({
 history: createWebHistory(),
 routes: [
 { path: '/', name: 'home', component: Home},
 { path: '/list', name: 'list', component: List},
 ],
});
export default router //musisz wyeksportować żeby zaimportować!
