const express = require("express");
const cors = require("cors");
const fs = require("fs"); //do odczytu pliku list.sql
const app = express();
const http = require('http').Server(app);
const io = require("socket.io")(http, { cors: { origin: "http://localhost:3000", methods: ["GET", "POST"] } }); //socket.io
const passport = require("passport");
const flash = require('express-flash')
const cookieParser = require("cookie-parser");
const bcrypt = require("bcryptjs");
const session = require("express-session");
const { pool } = require("./dbConfig");
const initializePassport = require('./passportConfig');

initializePassport(passport);

app.use(express.urlencoded({ extended: true }));
app.use(cors({credentials: true, origin: 'http://localhost:8081'}));
app.use(flash());
app.use(express.json());

app.use(session({
    secret: "secretcode",
    resave: false,
    saveUninitialized: true,
    cookie: {
    maxAge: 1000 * 60 * 60 * 24
    }
}));

app.use(cookieParser("secretcode"));
app.use(passport.initialize());
app.use(passport.session());

io.sockets.on("connection", () => {
    console.log("Socket works") //sprawdzenie w konsoli czy nasz socket działa
});
   

app.get('/', async (req, res) => {
    res.send('Hello world');
});

app.listen(8080, () => console.log("Listen to port 8080"));

require("dotenv").config();
const dbConnData = {
 host: process.env.PGHOST || "127.0.0.1",
 port: process.env.PGPORT || 5432,
 database: "postgres", //wcześniej stwórz w terminalu postgresa nową bazę danych o nazwie lab
 user: "postgres",
 password: "mysecretpassword"};
const {Client} = require("pg");
const client = new Client(dbConnData);
console.log("Connection parameters: ");
console.log(dbConnData);

client
 .connect()
 .then(() => {
    console.log("Connected to PostgreSQL");
    const port = process.env.PORT || 5000;
    const createTables = fs.readFileSync("./db.sql").toString();
    const insertData = fs.readFileSync("./insert.sql").toString();
    
    http.listen(port, () => {
        //  client.query(createTables);
        //  client.query(insertData);
        console.log(`API server listening at http://localhost:${port}`);
        });
    })
 .catch(err => console.error("Connection error", err.stack));

 app.get("/posts", async (req, res) => {
    const movies = (await client.query("SELECT * FROM post")).rows;
    res.send(movies);
  });